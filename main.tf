//render any additional conf files 
data "template_file" "env_yml" {
  template = "${path.module}/templates/env.yml.tpl"
  vars = {
    rds_host            = var.rds_host
    worldview_db_passwd = var.worldview_db_passwd
    api_endpoint        = var.api_endpoint
    refresh_token_url   = var.refresh_token_url
    app_client_id       = var.app_client_id
    app_client_secret   = var.app_client_secret
  }
}

resource "local_file" "env_yml_rendered" {
  filename = "${path.module}/env.yaml"
  content  = data.template_file.env_yml.rendered
}

data "template_file" "azure_db" {
  template = "${path.module}/templates/azure_database.json.tpl"
  vars = {
    rds_host = var.rds_host
  }
}

resource "local_file" "database_json" {
  filename = "${path.module}/database.json"
  content  = data.template_file.azure_db.rendered
}

//render nginx template
data "template_file" "nginx_conf" {
  template = file("${path.module}/templates/nginx.tpl")
  vars = {
    svc_endpoint = var.svc_endpoint
  }
}

resource "local_file" "nginx_conf" {
  filename = "${path.module}/nginx.conf"
  content  = data.template_file.nginx_conf.rendered
}

resource "local_file" "server_crt" {
  filename = "${path.module}/server.crt"
  content  = var.cert_body
}

resource "local_file" "server_key" {
  filename = "${path.module}/server.key"
  content  = var.cert_private_key
}

//execute packer build
resource "null_resource" "packer_build" {
  depends_on = [local_file.nginx_conf, local_file.env_yml_rendered]
  provisioner "local-exec" {
    environment = {
      PKR_VAR_aws_region   = var.aws_region
      PKR_VAR_namespace    = var.namespace
      PKR_VAR_stage        = var.stage
      PKR_VAR_svc_name     = "mgmt"
      PKR_VAR_svc_endpoint = var.svc_endpoint
      PKR_VAR_prefix       = var.prefix
    }
    command = "packer build ${path.module}/mgmt_host.pkr.hcl"
  }
}

// extract packer buile ami_name
data "external" "mgmt_ami" {
  depends_on = [null_resource.packer_build]
  program    = ["bash", "python3 ${path.module}/parse_ami_id.py manifest.json"]
  query = {
    "ami" = "ami_id"
  }
}

//provision elastic IP
resource "aws_eip" "mgmt" {
  instance = aws_instance.mgmt.id
  vpc      = true

  tags = {
    Name = "${var.namespace}-${var.prefix}-${var.stage}-${var.svc_name}"
  }
}

//launch new instance with provisioned packer AMI
resource "aws_instance" "mgmt" {
  ami                    = data.external.mgmt_ami.result.ami_id
  instance_type          = var.instance_type
  vpc_security_group_ids = var.security_groups
  subnet_id              = var.public_mgmt_subnet
  key_name               = var.aws_mgmt_key
  root_block_device {
    delete_on_termination = false
    encrypted             = true
    volume_size           = 100
  }

  tags = {
    Name = "${var.namespace}-${var.prefix}-${var.stage}-${var.svc_name}"
  }
}

//attach EIP to mgmt_host
resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.mgmt.id
  allocation_id = aws_eip.mgmt.id
}

//provision DNS records 
data "aws_route53_zone" "root" {
  name = var.root_domain
}

resource "aws_route53_record" "mgmt" {
  zone_id = data.aws_route53_zone.root.zone_id
  name    = "${var.namespace}-${var.prefix}-${var.stage}-${var.svc_name}"
  type    = "A"
  ttl     = "300"
  records = [aws_eip.mgmt.public_ip]
}


//provision docker-based jenkins install on mgmt instance install plugins, generate seed job


variable "aws_region" {}
variable "namespace" {}
variable "prefix" {}
variable "stage" {}
variable "root_domain" {}
variable "svc_name" {}
variable "svc_endpoint" {}
variable "mgmt_ami" {
  default = "ami-03109519244d2472a"
}
variable "rds_host" {}
variable "worldview_db_passwd" {}
variable "api_endpoint" {}
variable "refresh_token_url" {}
variable "app_client_id" {}
variable "app_client_secret" {}
variable "cert_body" {}
variable "cert_private_key" {}
variable "instance_type" {
  default = "t3.large"
}
variable "security_groups" {}
variable "public_mgmt_subnet" {}
variable "aws_mgmt_key" {}


import json
import sys


file = sys.argv[-1]

with open(file) as m:
  data = json.load(m)
  
ami_id = (data['builds'][-1]['artifact_id'][10:])
json_data = json.dumps({"ami": ami_id })

sys.stdout.write(json_data)
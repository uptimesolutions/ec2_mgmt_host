
locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "")
}

variable "svc_name" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "svc_endpoint" {
  type = string
}

variable "namespace" {
  type = string
}

variable "stage" {
  type = string
}

variable "prefix" {
  type = string
}

variable "instance_type" {
  type    = string
  default = "t3.large"
}

source "amazon-ebs" "base" {
  ami_name      = "${var.namespace}-${var.stage}-${var.svc_name}-${local.timestamp}"
  instance_type = var.instance_type
  region        = var.aws_region

  source_ami_filter {
    filters = {
      name                = "amzn2-ami-hvm-2.0.*-x86_64-gp2"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["amazon"]
  }
  ssh_username = "ec2-user"
}

build {
  sources = ["source.amazon-ebs.base"]

  provisioner "shell" {
    inline = [
      "sudo yum update -y",
      "sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo",
      "sudo yum install -y terraform packer",
      "sudo amazon-linux-extras install -y docker nginx1 ansible2 epel",
      "sudo yum install -y gcc-c++ tkinter python3-tkinter python3-pip python3-devel unixODBC-devel libsndfile",
      "sudo yum-config-manager --add-repo https://packages.microsoft.com/config/rhel/8/prod.repo",
      "sudo yum remove unixODBC-utf16 unixODBC-utf16-devel",
      "sudo ACCEPT_EULA=Y yum install -y msodbcsql17",
      "sudo ACCEPT_EULA=Y yum install -y mssql-tools",
      "curl -sL https://rpm.nodesource.com/setup_10.x | sudo bash -",
      "sudo yum install -y nodejs",
      "sudo python3 -m pip install --upgrade pip",
      "sudo python3 -m pip install virtualenv",
      "sudo yum install -y cifs-utils",
      //"sudo systemctl start docker && sudo systemctl start docker",
      "sudo amazon-linux-extras install java-openjdk11",
      "sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins.io/redhat-stable/jenkins.repo",
      "sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key",
      "sudo yum install -y jenkins java-1.8.0-openjdk",
      "sudo systemctl enable jenkins && sudo systemctl start jenkins",
      "sudo yum install -y git",
      "sudo mkdir -p /var/lib/jenkins/.env",
      "sudo chown -R jenkins:jenkins /var/lib/jenkins/.env",
      "sudo systemctl enable nginx",
    ]
  }

  provisioner "file" {
    source      = "./nginx.conf"
    destination = "/etc/nginx/nginx.conf"
  }

  provisioner "file" {
    source      = "./env.yaml"
    destination = "/var/lib/jenkins/.env/env.yaml"
  }

  provisioner "file" {
    source      = "./database.json"
    destination = "/var/lib/jenkins/.env/database.json"
  }

  provisioner "file" {
    source      = "./server.crt"
    destination = "/etc/nginx/ssl/server.crt"
  }

  provisioner "file" {
    source      = "./server.key"
    destination = "/etc/nginx/ssl/server.key"
  }

  post-processor "manifest" {
    output = "manifest.json"
  }
}

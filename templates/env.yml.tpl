---
verbose: true
clients:
  default:
    log:
      directory: "/var/log/WorldviewLogs/"
      verbose: true
      save_history: false
      level: "Debug"
      sql_logs: false
    database:
      ip: "${rds_host}"
      port: 1433
      driver: "ODBC Driver 17 for SQL Server"
      name: "worldview"
      username: "worldview"
      password: "${worldview_db_passwd}"
    inputs:
      directories:
        tach: "/opt/wv/tach"
        liveExports: "/opt/wv/liveExports"
        dcInput: "/opt/wv/scratch/"
        other: "/opt/wv/other"
    # filehandler:
    #   tokenExpirationMinutes: 30
    #   pythonApiUrl: "${api_endpoint}"
    #   refreshTokenUrl: "${refresh_token_url}"
    #   appClientId: "${app_client_id}"
    #   clientSecret: "${app_client_secret}"
    #   fileBackup: false
    #   ts1Dir: "C:/ts1"
    #   threads: 1
  worldview:
    api:
      threads: 12
      workers: 8
      priorityWorkers: 4
      wvservicehelperEnabled: false
      ts1filewatcherEnabled: false
      pollWaitSeconds: 10
      decimationWaitSeconds: 10
      flask_key: "E=2J33MHH^g!*iur8T?q2fg"
      ts1_path: "C:/Users/Public/Documents/TS1"
      migrate: false
      create: false
      fileBackup: false
      saveSpectrum: true
  azure:  
    database:
      ip: "${rds_host}"
      port: 1433
      driver: "ODBC Driver 17 for SQL Server"
      name: "uptime_ml"
      username: "worldview"
      password: "${worlview_db_passwd}"
    log:
      directory: "/var/log/WorldviewLogs/ML"
      verbose: true
      save_history: true
      sql_logs: false